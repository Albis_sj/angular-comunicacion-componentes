import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  parentMessage = 'Mensaje del padre'; //variable con un valor
  mensajePadre!: string
  constructor() { }

  ngOnInit(): void {
  }

  recibirMensaje ($event: string):void {
    this.mensajePadre = $event;
    console.log(this.mensajePadre);
  }

}
