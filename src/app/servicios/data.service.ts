import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  nombre:string = 'sin nombre';
  direccion: string = '';

  private _dataSource = new BehaviorSubject<string>(''); //BehaviorSubject es un objeto de tipo cadena, inicializado vacio

  //asObservable lo convierte a un observable
  //dataSource$ es una variable para suscribirse a la informacion, tambien se le puede subscribir, por eso el pesos
  dataSource$ = this._dataSource.asObservable(); //se usa this para llamas porque estamos en una clase, 
  constructor() { }

  public ModificarDireccion (_direccion: string): void{
    this.direccion = _direccion;
    //con next se coloca un valor al _daraSource (el valor direccion) es como colocar el igual.
    this._dataSource.next(this.direccion); //este _dataSource va hacia arriba, al $
  }
}
