import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentComponent } from './component/parent/parent.component';
import { ChildComponent } from './component/child/child.component';
import { PadreComponent } from './component/padre/padre.component';
import { HijoComponent } from './component/hijo/hijo.component';
import { HomeComponent } from './component/home/home.component';
import { BoxComponent } from './component/box/box.component';
import { PrincipalComponent } from './component/principal/principal.component';
import { SecundarioComponent } from './component/secundario/secundario.component';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    ChildComponent,
    PadreComponent,
    HijoComponent,
    HomeComponent,
    BoxComponent,
    PrincipalComponent,
    SecundarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
